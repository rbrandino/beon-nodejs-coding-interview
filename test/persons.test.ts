const assert = require('assert')
import { Person, PersonsModel } from '../src/models/persons.model'
import { db } from '../src/memory-database'
import { after, afterEach } from 'mocha'
import { PersonsService } from '../src/services/persons.service'

describe('Persons', () => {
    let service: any

    beforeEach(async () => {
        await db({ test: true })

        const persons = [
            {
                name: 'John Smith',
                gender: 'Male',
                email: 'john.smith@email.com',
                type: 'A',
            },
            {
                name: 'Matt Simons',
                gender: 'Male',
                email: 'matt@email.com',
                type: 'B',
            },
        ]

        persons.forEach(
            async (person: Person) => await PersonsModel.create(person)
        )

        service = new PersonsService()
    })

    afterEach(async () => {
        await PersonsModel.remove({})
    })

    describe('Persons Model', async () => {
        it('Allows to create two persons with different emails', async () => {
            const personsCountBefore = await PersonsModel.countDocuments({})
            assert.equal(personsCountBefore, 2)

            await service.create({
                name: 'Fake Name',
                gender: 'Male',
                email: 'fake@email.com',
                type: 'B',
            })

            const personsCountAfter = await PersonsModel.countDocuments({})
            assert.equal(personsCountAfter, 3)

            // Arrange
            // Act
            // Assert
        })

        it('Prevents creating a person that already exists on the Database', async () => {
            const personsCountBefore = await PersonsModel.countDocuments({})
            assert.equal(personsCountBefore, 2)

            const createFn = async () =>
                await service.create({
                    name: 'Fake Name',
                    gender: 'Male',
                    email: 'matt@email.com',
                    type: 'B',
                })

            assert.rejects(createFn, (err: any) => {
                assert(err instanceof Error)
            })

            const personsCountAfter = await PersonsModel.countDocuments({})
            assert.equal(personsCountAfter, 2)

            // Arrange
            // Act
            // Assert
        })
    })
})
