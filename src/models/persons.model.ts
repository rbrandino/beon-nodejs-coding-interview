import mongoose, { Schema } from 'mongoose'
import { Length, IsEmail, IsEnum, IsOptional } from 'class-validator'

enum Gender {
    FEMALE = 'Female',
    MALE = 'Male',
}

export class Person {
    @Length(1, 255, {
        message: '$property should have between $constraint1 and $constraint2',
        always: true,
    })
    name: string

    @IsEmail(undefined, { message: 'invalid $property', always: true })
    email: string

    @IsEnum(Gender, { message: 'invalid $property', always: true })
    gender: string

    @Length(1, 255, {
        message:
            '$property should have between $constraint1 and $constraint2 characters',
        always: true,
    })
    @IsOptional({ always: true })
    type: string
}

const schema = new Schema<Person>(
    {
        name: { required: true, type: String },
        email: { required: true, unique: true, type: String },
        gender: {
            required: true,
            type: String,
            enum: {
                values: ['Male', 'Female'],
                message: '{VALUE} gender is not supported',
            },
        },
        type: { type: String },
    },
    { timestamps: true }
)

export const PersonsModel = mongoose.model('Persons', schema)
