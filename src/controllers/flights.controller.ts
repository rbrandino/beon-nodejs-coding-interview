import { JsonController, Get, Post, Param, BodyParam } from 'routing-controllers'
import { FlightsService } from '../services/flights.service'

const flightsService = new FlightsService()

@JsonController('/flights')
export default class FlightsController {
    @Get('', { transformResponse: false })
    async getAll() {
        const data = await flightsService.getAll()
        return {
            status: 200,
            data,
        }
    }

    @Post('/:flightId/onboardPassenger', { transformResponse: false })
    async onboardPassenger(@Param('flightId') flightId: string, @BodyParam('passengerId') passengerId: string) {
      const data = await flightsService.onboardPassenger(flightId, passengerId);
      return {
        status: 200,
        data: data,
      };
    }
}
