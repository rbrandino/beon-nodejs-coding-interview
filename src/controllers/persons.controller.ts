import { JsonController, Param, Get, Post, Body, Delete } from 'routing-controllers'
import { Person } from '../models/persons.model'
import { PersonsService } from '../services/persons.service'

const personsService = new PersonsService()

@JsonController('/persons', { transformResponse: false })
export class PersonsController {
    @Get()
    async getAll() {
        const data = await personsService.getAll()
        return {
            status: 200,
            data,
        }
    }

    @Post('', { transformResponse: false })
    async create(@Body() person: Person) {
        const data = await personsService.create(person)
        return {
            status: 201,
            data
        }
    }

    @Delete('/:id', { transformResponse: false })
    async remove(@Param('id') id: string) {
        await personsService.remove(id)
        return {
            status: 204,
        }
    }
}
