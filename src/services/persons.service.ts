import { Person, PersonsModel } from '../models/persons.model'

export class PersonsService {
    async getAll() {
        return await PersonsModel.find()
    }

    async create(person: Person) {
        const personExists = await PersonsModel.findOne({ email: person.email })
        if (personExists) {
          console.log('Teste')
            throw new Error(`Person already exist`)
        }

        return await PersonsModel.create(person)
    }

    async remove(id: string) {
        const person = await PersonsModel.findOne({ _id: id })
        if (!person) throw new Error(`PersonId ${id} does not exist`)

        return await person.remove()
    }
}
