import { FlightsModel } from '../models/flights.model'
import { PersonsModel } from '../models/persons.model'

export class FlightsService {
    async getAll() {
        return await FlightsModel.find()
    }

    async onboardPassenger(flightCode: string, passengerId: string) {
        const flight = await FlightsModel.findOne({ code: flightCode })
        if (!flight) throw new Error(`Flight code ${flightCode} does not exist`)

        const person = await PersonsModel.findOne({ _id: passengerId })
        if (!person) throw new Error(`PersonId ${passengerId} does not exist`)

        const passengerExists = flight.passengers.findIndex(
            (element: any) => element._id.toString() === passengerId
        )

        if (passengerExists > -1) {
            throw new Error(
                `Passenger ${passengerId} already included in the flight`
            )
        }

        flight.passengers.push(person)
        return await flight.save()

        //return await FlightsModel.updateOne(
        //    { code: flightCode },
        //    { $set: { passengers: flight.passengers } }
        //)
    }
}
